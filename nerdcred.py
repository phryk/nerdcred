# coding=utf-8

from string import printable as printable_chars
from random import randint, choice
from hashlib import sha512
from functools import wraps
from datetime import datetime

import peewee as d#atabase
from flask import Flask, g, request, session, flash, render_template, redirect, abort, url_for
from flask.ext.sass import Sass

db = d.PostgresqlDatabase('nerdcred', user='test', password='test', threadlocals=True)
app = Flask('nerdcred')

import config
Sass(app)




class Model(d.Model):

    class Meta:
        database = db




class User(Model):

    name = d.CharField(unique=True)
    password = d.CharField()
    salt = d.CharField()

    passhash = None
    score = None


    @classmethod
    def login(cls, name, password):

        fail = False

        try:

            user = cls.get(User.name == name)

            if sha512(password + user.salt).hexdigest() == user.passhash:
                return user

            else:
                fail = True

        except Exception as e:

            if type(e) is cls.DoesNotExist:
                fail = True

            else:
                raise e

        finally:

            if fail:
                raise Exception('Wrong username or password.')


    @classmethod
    def get(cls, *query, **kwargs):

        user = super(User, cls).get(*query, **kwargs)
        user.passhash = user.password
        user.password = None

        user.score = Transaction.select(d.fn.SUM(Transaction.value)).where(Transaction.recipient == user)[0].sum

        return user


    def save(self, force_insert=False, only=None):

        if self.password:
            self.salt = ''.join(choice(printable_chars) for i in range(randint(32, 48)))
            self.password = sha512(self.password + self.salt).hexdigest()
            self.passhash = self.password

        super(User, self).save(force_insert, only)




class Transaction(Model):

    created = d.DateTimeField()
    sender = d.ForeignKeyField(User, related_name='_sender')
    recipient = d.ForeignKeyField(User, related_name='_recipient')
    value = d.IntegerField()
    message = d.CharField()




# Utility functions

def render(content, **kwargs):

    """
        Render the template named by `content` within boilerplate.jinja
    """

    return render_template('boilerplate.jinja', content=content, title=g.title, **kwargs)




# Custom decorators

def load_user(func):

    @wraps(func)
    def decorator(*args, **kwargs):

        if kwargs.has_key('user'):

            try:

                user = User.get(User.name == kwargs['user'])
                kwargs['user'] = user

            except User.DoesNotExist:

                abort(404)


        return func(*args, **kwargs)

    return decorator


def logged_in(func):

    @wraps(func)
    def decorator(*args, **kwargs):

        if g.user is not None:

            return func(*args, **kwargs)

        abort(403, 'You must be logged in for this, mate.')

    return decorator





# Initialization

@app.before_request
def before_request():

    if request.endpoint != 'page_install':

        db.connect()
        if session.has_key('uid'):

            try:
                g.user = User.get(User.id == session['uid'])

            except Exception as e:
                session.pop('uid')
                g.user = None

        else:
            g.user = None


        g.title = 'Nerdcred'




# Routes

@app.route('/install')
def page_install():

    g.title = 'Installation'

    flash('TODO: Authorization for this page', 'todo')

    try:
        User.create_table()


    except Exception as e:
        flash('User table could not be created.', 'error')
        flash(e.message)

    else:

        jehova = User()
        jehova.name = 'JHVH-9001'
        jehova.password = ''.join(choice(printable_chars) for i in range(randint(32, 48)))
        jehova.salt="foo"
        jehova.save()


    try:
        Transaction.create_table()
    except Exception as e:
        flash('Transaction table could not be created.', 'error')
        flash(e.message)


    flash('Installation procedure executed.')
    return redirect('/')


@app.route('/')
def page_home():

    g.title = "Well, that's just, like, your opinion, man."

    return render('home.jinja')




@app.route('/register')
def page_register():

    g.title = 'Registration'

    return render('registration.jinja')


@app.route('/register', methods=['POST'])
def post_register():

    try:

        user = User()
        user.name = request.form['name']
        user.password = request.form['password']

        user.save()

    except Exception as e:

        flash("Couldn't save user.", 'error')
        flash(e.message)
        location = '/register'

    else:

        flash('Successfully registered!')
        location = '/'

    finally:

        return redirect(location)
    



@app.route('/login')
def page_login():

    g.title = 'Login'

    if g.user is None:
        return render('login.jinja')

    else:
        flash("You're already logged in as %s, dude." % (g.user.name,))
        return redirect('/')


@app.route('/login', methods=['POST'])
def post_login():

    try:
        user = User.login(name=request.form['name'], password=request.form['password'])

    except Exception as e:

        flash("Couldn't log you in.", 'error')
        flash(e.message)
        location = '/login'

    else:

        session['uid'] = user.id
        location = '/'

    return redirect(location)




@app.route('/logout')
def page_logout():

    if g.user is not None:

        name = g.user.name

        g.user = None
        session.pop('uid')
        flash('Logged out %s' % (name,))

    else:

        flash("I can't log you out, since you're not logged in, man.")

    return redirect('/')




@app.route('/<string:user>/')
@load_user
def page_user(user):

    g.title = user.name
    return render('user.jinja', user=user)




@app.route('/<string:user>/transaction')
@logged_in
@load_user
def page_transaction(user):

    return render('transaction.jinja')


@app.route('/<string:user>/transaction', methods=['POST'])
@logged_in
@load_user
def post_transaction(user):

    transaction = Transaction()

    transaction.created = datetime.now()
    transaction.sender = g.user
    transaction.recipient = user
    transaction.message = request.form['message']

    if request.form['value'] == '1':
        transaction.value = 1

    elif request.form['value'] == '-1':
        transaction.value = -1

    else: # Cheat and I'll fucking cut you!

        transaction.sender = User.get(User.id == 1)
        transaction.recipient = g.user
        transaction.message = "Cheating motherfuckers be stoned lol."
        transaction.value = abs(request.form['value']) * -1
        flash("lololol, haxx0r!", 'fuckhead')

    transaction.save()

    return redirect(url_for('.page_user', user=user.name))
